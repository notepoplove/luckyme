import React, {Component} from 'react';
import {Text, Image, TouchableOpacity} from 'react-native';
import UIPageLayout from '../containers/UIPageLayout';
import {strings, switchLanguage} from '../../components/i18n';

class UISearch extends Component {
  static navigationOptions = {
    title: 'Home',
  };
  state = {
    search: null,
  };

  updateSearch = search => {
    this.setState({search});
  };

  onSetRuLang = language => {
    switchLanguage(language, this);
  };

  render() {
    const {navigation} = this.props;
    const otherParam = navigation.getParam('otherParam', 'some default value');
    const {search} = this.state;
    return (
      <UIPageLayout
        updateSearch={this.updateSearch}
        search={search}
        onSetRuLang={this.onSetRuLang}>
        <Text>UISearch{search}</Text>
        <Text>{strings('greeting')}</Text>
        <Text>otherParam: {JSON.stringify(otherParam)}</Text>
        <TouchableOpacity
          activeOpacity={1}
          onPressOut={() => {
            console.log('FR');
            // I18n.locale = 'fr';
            // React.component.forceUpdate();
            // this.forceUpdate();
            this.onSetRuLang('fr');
          }}>
          <Image
            source={require('../asset/search_black.png')}
            style={{width: 50, height: 50}}
          />
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={1}
          onPressOut={() => {
            console.log('en');
            // I18n.locale = 'en';
            // this.forceUpdate();
            this.onSetRuLang('en');
          }}>
          <Image
            source={require('../asset/search_black.png')}
            style={{width: 50, height: 50}}
          />
        </TouchableOpacity>
      </UIPageLayout>
    );
  }
}

export default UISearch;
