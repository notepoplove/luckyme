import React, {Component} from 'react';
import {View, Button} from 'react-native';
import UIPageLayout from '../containers/UIPageLayout';

class UISetting extends Component {
  render() {
    return (
      <UIPageLayout>
        <Button
          title="Go to Details 2"
          onPress={() => this.props.navigation.navigate('Details')}
        />
      </UIPageLayout>
    );
  }
}

export default UISetting;
