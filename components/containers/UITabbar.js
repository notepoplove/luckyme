import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import {createAppContainer} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import UISearch from '../pages/UISearch';
import UIMyShop from '../pages/UIMyShop';
import UISettings from '../pages/UISettings';
import UIDetailsScreen from '../pages/UIDetailsScreen';
import {createStackNavigator} from 'react-navigation-stack';

const StackUISearch = createStackNavigator(
  {
    UISettings: {
      screen: UISettings,
      navigationOptions: {
        header: null,
      },
    },
    Details: UIDetailsScreen,
  },
  {
    navigationOptions: {},
  },
);

export default createAppContainer(
  createBottomTabNavigator(
    {
      Search: {
        screen: UISearch,
        navigationOptions: {
          tabBarIcon: ({tintColor}) => (
            <Ionicons name="md-search" color={tintColor} size={24} />
          ),
        },
      },
      'My Shop': {
        screen: UIMyShop,
        navigationOptions: {
          tabBarIcon: ({tintColor}) => (
            <Entypo name="shop" color={tintColor} size={24} />
          ),
        },
      },
      Settings: {
        screen: StackUISearch,
        navigationOptions: {
          tabBarIcon: ({tintColor}) => (
            <Ionicons name="md-settings" color={tintColor} size={24} />
          ),
        },
      },
    },
    {
      initialRouteName: 'Search',
      order: ['Search', 'My Shop', 'Settings'],
      tabBarOptions: {
        activeTintColor: '#ffffff',
        inactiveTintColor: '#263238',
        style: {
          backgroundColor: '#FF6F00',
        },
      },
    },
  ),
  {},
);
