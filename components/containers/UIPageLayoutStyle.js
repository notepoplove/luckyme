'use strict';
import {StyleSheet} from 'react-native';

module.exports = StyleSheet.create({
  containerToolBar: {
    backgroundColor: '#FF6F00',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 8,
    paddingRight: 8,
    height: 56,
  },
  searchBarinputContainer: {
    backgroundColor: '#ffffff',
    height: 32,
  },
  searchBarContainer: {
    backgroundColor: '#FF6F00',
    flex: 1,
    borderBottomWidth: 0,
    borderTopWidth: 0,
    marginRight: 20,
    marginLeft: 28,
  },
  imageLanguage: {
    width: 32,
    height: 32,
    borderRadius: 50,
    borderColor: '#ffffff',
    borderWidth: 0.3,
  },

  modalContainer: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewModal: {
    backgroundColor: '#ffffff',
    display: 'flex',
    flexDirection: 'row',
    borderRadius: 5,
    shadowColor: '#30C1DD',
    shadowOpacity: 0.6,
    shadowRadius: 10,
    elevation: 3,
  },
  flag: {
    width: 50,
    height: 50,
    margin: 8,
  },
});
