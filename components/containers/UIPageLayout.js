import React, {Component} from 'react';
import {
  SafeAreaView,
  View,
  Image,
  TouchableOpacity,
  Modal,
  Text,
} from 'react-native';
import {SearchBar} from 'react-native-elements';
// import I18n from 'react-native-i18n';
import Ionicons from 'react-native-vector-icons/Ionicons';
import styles from './UIPageLayoutStyle';
import {strings} from '../../components/i18n';

class UIPageLayout extends Component {
  constructor(props) {
    super(props);
    console.log(':>', this.props);
  }
  state = {
    modalVisible: false,
    languageVisible: require('../asset/TH.png'),
  };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  setLanguageVisible(visible) {
    this.setState({languageVisible: visible});
  }

  render() {
    return (
      <SafeAreaView>
        <View style={styles.containerToolBar}>
          <Ionicons name="ios-menu" color="#ffffff" size={32} />
          <SearchBar
            round
            inputContainerStyle={styles.searchBarinputContainer}
            containerStyle={styles.searchBarContainer}
            placeholder="search"
            onChangeText={this.props.updateSearch}
            value={this.props.search}
          />
          <TouchableOpacity
            onPress={() => {
              console.log('L:' + this.state.languageVisible);
              this.setModalVisible(!this.state.modalVisible);
            }}>
            <Image
              source={this.state.languageVisible}
              style={styles.imageLanguage}
            />
          </TouchableOpacity>
        </View>
        {this.props.children}
        <Text>{strings('greeting')}</Text>
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setModalVisible(false);
          }}>
          <TouchableOpacity
            activeOpacity={1}
            onPressOut={() => {
              this.setModalVisible(!this.state.modalVisible);
            }}>
            <View style={styles.modalContainer}>
              <View style={styles.viewModal}>
                <TouchableOpacity
                  onPressOut={() => {
                    this.props.onSetRuLang('fr');
                    this.setLanguageVisible(require('../asset/TH.png'));
                    this.setModalVisible(!this.state.modalVisible);
                  }}>
                  <Image
                    source={require('../asset/TH-R.png')}
                    style={styles.flag}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  onPressOut={() => {
                    this.props.onSetRuLang('en');
                    this.setLanguageVisible(require('../asset/UK.png'));
                    this.setModalVisible(!this.state.modalVisible);
                  }}>
                  <Image
                    source={require('../asset/UK-R.png')}
                    style={styles.flag}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </TouchableOpacity>
        </Modal>
      </SafeAreaView>
    );
  }
}

export default UIPageLayout;
