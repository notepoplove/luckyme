/**
 * @format
 */

import {AppRegistry} from 'react-native';
import UITabbar from './components/containers/UITabbar';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => UITabbar);
